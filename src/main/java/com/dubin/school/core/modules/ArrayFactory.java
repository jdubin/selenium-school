package com.dubin.school.core.modules;
import java.util.Arrays;
/** Issue #7 - Create class ArrayFactory
 *
 */


public class ArrayFactory {

    int a;
    String name;

    public ArrayFactory(int a, String name) {
        this.a = a;
        this.name = name;
    }
//getArray - which return array filled with numbers from 1 -> a

    public int[] getArray() {
        int[] tab = new int[a];
        for (int i = 0; i < a; i++) {
        tab[i]=i+1;
        }
            return tab;
    }

    public int[] getReverseArray() {
        int[] newTab = new int[a];
        for (int j = 0; j < a; j++) {
            newTab[j] = a-j;
        }
        return newTab;
    }

//getName -  which return name string

    public String getName () {
        return this.name;
    }
//which return string in reverse order (eg. if name == "jurek" then return "keruj")

  public String getReverseName() {
      char[] newWord = new char[name.length()];
      for (int i = 0, j = name.length() - 1; i < name.length(); i++, j--) {
          newWord[j] = name.charAt(i);
      }
      return new String(newWord);
  }

}