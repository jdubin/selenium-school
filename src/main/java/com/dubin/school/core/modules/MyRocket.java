package com.dubin.school.core.modules;

import com.dubin.school.core.modules.server.Figure;

public class MyRocket implements Figure {

    int sum;
    int dif;
    int multi;


    public MyRocket() {
    }

    /**
     * Issue #1 - getMyAge method return current age.
     */
    public void getMyAge(int age) {
        System.out.println("I am " + age + " years old.");
    }

    /**
     * Issue #2 - Create method calculate with parameters
     *
     * @param a
     * @param b
     */
    public void calculation(int a, int b) {
        sum = a + b;
        dif = a - b;
        multi = a * b;
        System.out.println("Addition: " + sum);
        System.out.println("Substraction: " + dif);
        System.out.println("Multiplication: " + multi);
    }

    /**
     * Issue #3 - Create method checkIfEven
     *
     * @param a
     */

    public boolean checkIfEven(int a) {
        if (a % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Issue #4 - Create method for checking if number is divided by 3 and 5.
     *
     * @param a
     */

    public boolean checkingNumber(int a) {
        if (a % 3 == 0 && a % 5 == 0) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Issue #5 - Create method power
     *
     * @param a
     */
    public void powerMath(double a) {
        System.out.println("Math.pow(" + a + "," + a + ")=" + Math.pow(a, a));
    }

    @Override
    public void getSurfaceArea(String shape) {

    }

    @Override
    public void getCircuit(String type) {

    }
}





