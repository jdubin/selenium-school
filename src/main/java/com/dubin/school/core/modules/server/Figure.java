package com.dubin.school.core.modules.server;


/*Create interface Figure inside package com.dubin.school.core.modules.interface and two objects implemeting this interface Square and Circle.
        Objects should have constructor which initiate objects(with proper data).
interface should have two methods :
        getSurfaceArea() - based on data which you set inside constructor
        getCircuit() - based on data which you set inside constructor*/

public interface Figure {
    public void getSurfaceArea(String shape);
    public void getCircuit(String type);
}

